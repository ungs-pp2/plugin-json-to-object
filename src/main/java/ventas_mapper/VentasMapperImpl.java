package ventas_mapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.grupo6.model.Producto;
import com.grupo6.model.Venta;
import com.grupo6.utils.VentasMapper;

public class VentasMapperImpl implements VentasMapper {

	private String path;

	public VentasMapperImpl(){
		this.path = "";
	}

	@Override
	public void write(Venta venta, List<Venta> ventas) {
		try {
			HashMap<String, List<Venta>> list = new HashMap<>();
			ventas.add(venta);
			list.put("venta", ventas);
			new ObjectMapper().writeValue(new File(path), list);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public VentasMapper setPath(String s) {
		this.path = s;
		return this;
	}

	@Override
	public List<Venta> read() {
		List<Venta> ventas = new ArrayList<>();
		try {
			File file = new File(path);
			if (file.exists() && file.length() > 0) {
				final JsonNode arrNode = new ObjectMapper().readTree(file).get("venta");
				if (arrNode.isArray()) {
					for (final JsonNode objNode : arrNode) {
						Venta venta = new Venta();
						venta.setTotalVenta(Double.parseDouble(objNode.get("totalVenta") + ""));
						venta.setFechaVenta(objNode.get("fechaVenta") + "");
						List<Producto> productos = new ArrayList<>();
						for (final JsonNode objNodeProd : objNode.get("productos")) {
							Producto producto = new Producto();
							producto.setIdProducto(objNodeProd.get("idProducto") + "");
							producto.setDescripcion(objNodeProd.get("descripcion") + "");
							producto.setPrecioUnitario(Double.parseDouble(objNodeProd.get("precioUnitario") + ""));
							productos.add(producto);
						}
						venta.setProductos(productos);
						ventas.add(venta);
					}
				}
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ventas;
	}
}
