package ventas_mapper;

import com.grupo6.model.Venta;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class VentasMapperImplTest {

    @Test
    public void readNotFails() {
        List<Venta> ventas = new VentasMapperImpl().setPath("src/main/test/resources/ventas.json").read();
        Assertions.assertTrue(!ventas.isEmpty());
    }
}